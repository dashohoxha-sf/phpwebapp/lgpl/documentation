<?
//The $tab_items array contains the items of the tab. The values 
//of this array are used as labels for the items of the tab, and 
//the keys are used to generate the name of the template that
//contains the subform. E.g, when the item "Clinical Assessment"
//is clicked, the subform "clinicalAssesment.html" is loaded in 
//the page.

$tab_items = array(
	"intro" 	=> "Intro",
	"templates"	=> "Templates",
	"transitions"	=> "Transitions",
	"webox"		=> "WebBox-es",
	"events"	=> "Events",
	"variables"	=> "Variables",
	"session"	=> "Session",
	"database"	=> "Database",
	"new_app"	=> "New Application",
	"boxes"		=> "Common Weboxes"
);
?>