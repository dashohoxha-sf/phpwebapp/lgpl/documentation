<?
function mainMenu_eventHandler($event)
{
	global $session;

	switch ($event->name)
	{
		case "select":
			$menu_tab = $event->args["tab"];
			$session->Vars["mainMenu_tab"] = $menu_tab;
			break;
	}
}

function mainMenu_onLoad()
{
	global $session;

	//if no tab is selected, select one
	if (!isset($session->Vars["mainMenu_tab"]))
	{
		$session->addVar("mainMenu_tab", "about");
	}

	//set the {{content_file}} according to the selected tab
	switch ($session->Vars["mainMenu_tab"])
	{
		default:
		case "tutorial":
			$session->Vars["content_file"] = "tutorial/tutorial.html";
			break;
		case "user_manual":
			$session->Vars["content_file"] = "user_manual/user_manual.html";
			break;
		case "developer_docs":
			$session->Vars["content_file"] = "developer_docs.html";
			break;
		case "about":
			$session->Vars["content_file"] = "about.html";
			break;
	}
}

function mainMenu_onRender()
{
	//add backgroung variables for the tabs
	WebApp::addVars( array(
		"bg_tutorial"		=> "#EECCCC",
		"bg_user_manual"	=> "#EECCCC",
		"bg_developer_docs"	=> "#EECCCC",
		"bg_about"		=> "#EECCCC"

	));

	//set another color for the selected tab
	global $session;
	$menu_tab = $session->Vars["mainMenu_tab"];
	WebApp::addVar("bg_".$menu_tab, "#FFAAAA");
}
?>
