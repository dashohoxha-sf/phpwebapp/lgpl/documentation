<?
//The $tab_items array contains the items of the tab. The values 
//of this array are used as labels for the items of the tab, and 
//the keys are used to generate the name of the template that
//contains the subform. 

$tab_items = array(
	"intro" 	=> "Introduction",
	"transitions"	=> "1-Transitions",
	"templates"	=> "2-Templates",
	"weboxes"	=> "3-WebBox-es",
	"events"	=> "4-Events",
	"sess_var"	=> "5-Session and Variables",
	"database"	=> "6-Database",
	"advanced"	=> "7-Advanced"
);
?>
