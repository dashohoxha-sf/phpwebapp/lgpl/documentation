<?
function tutorialTab_eventHandler($event)
{
	global $session;
	
	switch ($event->name)
	{
		case "selectTab":
			$tab_id = $event->args["tab_id"];
			$session->Vars["tutorialSelectedTab"] = $tab_id;
			break;
	}
}

function tutorialTab_onLoad()
{
	global $session;
	if (!isset($session->Vars["tutorialSelectedTab"]))
	{
		//read the tab items
		$current_path = WebApp::getVar("./");
		include $current_path."tutorialTab_items.php";
		//set the first tab item as selected
		$first_tab_item = key($tab_items);
		$session->Vars["tutorialSelectedTab"] = $first_tab_item;
	}
}

function tutorialTab_onRender()
{
	$tab_table = get_tab_table();
	WebApp::addVar("tutorialTab_table", $tab_table);
}

function get_tab_table()
{
	global $session;
	$selected = $session->Vars["tutorialSelectedTab"];
	
	//read the tab items
	$current_path = WebApp::getVar("./");
	include TPL_PATH."tutorial/tutorialTab/tutorialTab_items.php";

	//open the table
	$tab_table .= "<table border='0' cellpadding='0' cellspacing='0' width='100%' bgcolor='white'>\n";

	//genrate first row of the table
	$tab_table .= "\t<tr>\n";
	reset($tab_items);
	while ( list($id,$item) = each($tab_items) )
	{
		$bgcolor1 = (($id==$selected or $prev_id==$selected) ? "#a5a6a5" : "white" );
		$img1 = (($id==$selected or $prev_id==$selected) ? "<img src='".GRAPHICS_PATH."darkgrey.gif' border='0'>" : "" );
		$bgcolor2 = ($id==$selected ? "#a5a6a5" : "white");
		$img2 = ($id==$selected ? "<img src='".GRAPHICS_PATH."darkgrey.gif' border='0'>" : "");
		$tab_table .= "\t\t<td bgcolor='".$bgcolor1."' width='1' height='2'>".$img1."</td>\n";
		$tab_table .= "\t\t<td bgcolor='".$bgcolor2."' width='10%' height='2'>".$img2."</td>\n";

		$prev_id = $id;
	}
	$bgcolor1 = (($id==$selected or $prev_id==$selected) ? "#a5a6a5" : "white" );
	$img1 = (($id==$selected or $prev_id==$selected) ? "<img src='".GRAPHICS_PATH."darkgrey.gif' border='0'>" : "" );
	$tab_table .= "\t\t<td bgcolor='".$bgcolor1."' width='1' height='2'>".$img1."</td>\n";
	$tab_table .= "\t\t<td bgcolor='white'><img src='".GRAPHICS_PATH."white.gif' border='0'></td>\n";
	$tab_table .= "\t</tr>\n";

	//generate second row of the table
	$tab_table .= "\t<tr>\n";
	reset($tab_items);
	while ( list($id,$item) = each($tab_items) )
	{
		$bgcolor1 = (($id==$selected or $prev_id==$selected) ? "#a5a6a5" : "white" );
		$img1 = (($id==$selected or $prev_id==$selected) ? "darkgrey.gif" : "white.gif" );
		$bgcolor2 = ($id==$selected ? "white" : "#cecfce");
		$tab_table .= "\t\t<td bgcolor='".$bgcolor1."' width='1' height='20'><img src='".GRAPHICS_PATH.$img1."' border='0'></td>\n";
		$tab_table .= "\t\t<td bgcolor='".$bgcolor2."' width='10%' align='center' nowrap='true'><font class='doclauftext'><a href=\"javascript: tutorialTab('".$id."')\" style='text-decoration:none; color:#000000'>".$item."</a></font></td>\n";
		$prev_id = $id;
	}
	$bgcolor1 = (($id==$selected or $prev_id==$selected) ? "#a5a6a5" : "white" );
	$img1 = (($id==$selected or $prev_id==$selected) ? "darkgrey.gif" : "white.gif" );
	$tab_table .= "\t\t<td bgcolor='".$bgcolor1."' width='1' height='20'><img src='".GRAPHICS_PATH.$img1."' border='0'></td>\n";
	$tab_table .= "\t\t<td bgcolor='#cecfce'>&nbsp;</td>\n";
	$tab_table .= "\t</tr>\n";

	//generate third row of the table
	$tab_table .= "\t<tr>\n";
	reset($tab_items);
	while ( list($id,$item) = each($tab_items) )
	{
		$bgcolor2 = ($id==$selected ? "white" : "#a5a6a5");
		$img2 = ($id==$selected ? "" : "<img src='".GRAPHICS_PATH."darkgrey.gif' border='0'>");
		$tab_table .= "\t\t<td bgcolor='#a5a6a5' width='1' height='2'><img src='".GRAPHICS_PATH."darkgrey.gif' border='0'></td>\n";
		$tab_table .= "\t\t<td bgcolor='".$bgcolor2."' width='10%' height='2'>".$img2."</td>\n";
		$prev_id = $id;
	}
	$tab_table .= "\t\t<td bgcolor='#a5a6a5' width='1' height='2'><img src='".GRAPHICS_PATH."darkgrey.gif' border='0'></td>\n";
	$tab_table .= "\t\t<td bgcolor='#a5a6a5'><img src='".GRAPHICS_PATH."darkgrey.gif' border='0'></td>\n";
	$tab_table .= "\t</tr>\n";
	
	//close the table
	$tab_table .= "</table>\n";
	
	return $tab_table;
}
?>
