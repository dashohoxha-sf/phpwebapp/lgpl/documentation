<?
//this function is called before any page is constructed
function on_beforePage()
{
	//measure the time of page generation
	global $timer;
	$timer->Start("WebApp", "Time that spends the application for constructing the page");
}
?>