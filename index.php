<?
include "application.php";

if ($event->targetPage=="undefined")
{
	//first time that the user enters in the application
	$fname = EVENTHANDLER_PATH."on.firstTime.php";
	if (file_exists($fname))
	{
		include $fname;
		on_firstTime();
	}
}

if ($event->target=="none")
{
	//call the free event
	WebApp::callFreeEvent($event);
}

//construct the target page of the event
$targetPage = $event->targetPage;
$tpl_file = TPL_PATH.$event->targetPage;
WebApp::constructHtmlPage($tpl_file);
?>
