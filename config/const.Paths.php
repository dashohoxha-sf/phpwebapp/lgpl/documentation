<?
//constants of the paths in the application
define("APP_URL",		"/lgpl/documentation/");
define("WEBAPP_PATH",		APP_PATH."../web_app/");
define("EVENTHANDLER_PATH",	APP_PATH."event_handlers/");

define("GRAPHICS_PATH",		APP_URL."graphics/");
define("TPL_PATH",		APP_PATH."templates/");

//paths of the sample application templates
define("APP1_PATH",		APP_PATH."../app1/");
define("APP2_PATH",		APP_PATH."../app2/");
define("APP1_URL",		"/lgpl/app1/");
define("APP2_URL",		"/lgpl/app2/");
?>
