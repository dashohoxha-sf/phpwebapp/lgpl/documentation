<?
$app_path = dirname(__FILE__);
$app_path = str_replace("\\", "/", $app_path);	//could be a Windows path
define("APP_PATH",		$app_path."/");

define("CONFIG_PATH",	APP_PATH."config/");
include CONFIG_PATH."const.Paths.php";

//include configuration constants
include CONFIG_PATH."const.Settings.php";
if (USES_DB)	include CONFIG_PATH."const.DB.php";

//include the WebApp framework
include WEBAPP_PATH."WebApp.php";
?>
